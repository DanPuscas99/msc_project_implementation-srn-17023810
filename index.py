from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer, ChatterBotCorpusTrainer
from flask import Flask, render_template, request

app = Flask(__name__)

bot = ChatBot("chatbot", read_only=False,
    logic_adapters=[{
                     "import_path":"chatterbot.logic.BestMatch",
                      "default_response":"Sorry I don't have an answer",
                      "maximum_similarity_treshold": 0.9
                    }
                    ])

#----------------- Bible Patterns ---------------------------------------------------------#
listToTrain_aboutGod = [
                "Who is God ",
                " 1 John 4:8 Whoever does not love does not know God, because God is love.",
                "Does God exist? ",
                "there are multiple arguements for the existence here is from a human experience view: Psalm 19:1 MSG version 2 God’s glory is on tour in the skies,  God-craft on exhibit across the horizon.Madame Day holds classes every morning, Professor Night lectures each evening. For more information visit: https://www.youtube.com/watch?v=ukOTW-pxuL0&ab_channel=GotQuestionsMinistries ",
                "what is god ",
                "Tozer (Aiden Wilson Tozer, American christian pastor) is right in that we cannot know what God is with respect to Himself. The book of Job declares, “Can you discover the depths of God? Can you discover the limits of the Almighty? They are high as the heavens, what can you do? Deeper than Sheol, what can you know?” (Job 11:7–8). for more information visit:https://www.gotquestions.org/what-is-God.html ",
                "is there a god",
                "The Bible says there is a God, that nature demonstrates a Creator (Psalm 19:1), and that God reveals enough of Himself in the world for people to know of Him Romans 1:20 NIV says For since the creation of the world God’s invisible qualities—his eternal power and divine nature—have been clearly seen, being understood from what has been made, so that people are without excuse. More information check: https://www.gotquestions.org/is-there-a-God.html",
                "is God real?",
                "Romans 1:20 NIV says For since the creation of the world God’s invisible qualities—his eternal power and divine nature—have been clearly seen, being understood from what has been made, so that people are without excuse. more information: https://www.gotquestions.org/is-God-real.html",
                "Does god make mistakes",
                "God makes no mistakes. His perfection and greatness disallow mistakes: “Great is the LORD and most worthy of praise; his greatness no one can fathom.” (Psalm 145:3). More information: https://www.gotquestions.org/does-God-make-mistakes.html",
                "Why does God allow sickness?",
                "The issue of sickness is always a difficult one to deal with. The key is remembering that God’s ways are higher than our ways (Isaiah 55:9). More information: https://www.gotquestions.org/God-allow-sickness.html",
                "does god love everyone or just christians",
                "There is a sense in which God loves everyone in the whole world (John 3:16; 1 John 2:2; Romans 5:8). More information: https://www.gotquestions.org/does-God-love-everyone.html",
                "why does god require faith",
                "Our relationship with God is similar to our relationship with others in that all relationships require faith. We can never fully know any other person. We cannot experience all they experience nor enter into their minds to know what their thoughts and emotions are. Proverbs 14:10 says, The heart knows its own bitterness, and a stranger does not share its joy"
]
# this provides training data and patterns for the Love topic of the Bible
listToTrain1_Love =[
                    "I feel unloved",
                    "Jhon 3:16 MSG says that For God so loved the world, that he gave he's only son, that whoever believes in him should not perish but have eternal life. ",
                    "what is the love of God ",
                    "in the old testement before Jesus came on earth the in Exdous 34:6 And he passed in front of Moses, proclaiming, “The Lord, the Lord, the compassionate and gracious God, slow to anger abounding in love and faithful and in the New testement 1 John 4:8 says Whoever does not love does not know God, because God is love.",
                    "does god love me ",
                    "Romans 5:6-8 NIV 6 You see, at just the right time, when we were still powerless, Christ died for the ungodly. 7 Very rarely will anyone die for a righteous person, though for a good person someone might possibly dare to die. 8 But God demonstrates his own love for us in this: While we were still sinners, Christ died for us.",
                    "i cant feel gods love ",
                    "Ephesians 3:17-19 NIV 17 so that Christ may dwell in your hearts through faith. And I pray that you, being rooted and established in love, 18 may have power, together with all the Lord’s holy people, to grasp how wide and long and high and deep is the love of Christ, 19 and to know this love that surpasses knowledge—that you may be filled to the measure of all the fullness of God.",
                    "people dont love me ",
                    "Jhon 3:16 MSG says that For God so loved the world, that he gave he's only son, that whoever believes in him should not perish but have eternal life. "
                    ]
listToTrain_Fear = [
                "i'm fearful about this situation in my life ",
                "Psalms 23:4 NIV Even though I walk through the darkest valley, I will fear no evil,for you are with me; your rod and your staff, they comfort me.",
                "I'm afraid about this thing im facing",
                "Psalm 27:1 NIV The Lord is my light and my salvation whom shall I fear? The Lord is the stronghold of my life of whom shall I be afraid?",
                "I'm so anxious",
                "but those who hope in the Lord  will renew their strength. They will soar on wings like eagles; they will run and not grow weary, they will walk and not be faint",
                "I'm fearful god",
                "Psalms 23:4 NIV Even though I walk through the darkest valley, I will fear no evil,for you are with me; your rod and your staff, they comfort me."
               ]

listToTrain_Depression = [
                          "what does the bible say about depression",
                          "8 Keep this Book of the Law always on your lips; meditate on it day and night, so that you may be careful to do everything written in it. Then you will be prosperous and successful. 9 Have I not commanded you? Be strong and courageous. Do not be afraid; do not be discouraged, for the Lord your God will be with you wherever you go.” Joshua 1:8-9 NIV",
                          "I feel low",
                          "3 Not only so, but we also glory in our sufferings, because we know that suffering produces perseverance; 4 perseverance, character; and character, hope. 5 And hope does not put us to shame, because God’s love has been poured out into our hearts through the Holy Spirit, who has been given to us. Romans 5:3-5 NIV",
                          "I feel like nothing makes sense",
                          "8 Keep this Book of the Law always on your lips; meditate on it day and night, so that you may be careful to do everything written in it. Then you will be prosperous and successful. 9 Have I not commanded you? Be strong and courageous. Do not be afraid; do not be discouraged, for the Lord your God will be with you wherever you go.” Joshua 1:8-9 NIV"
]

listToTrain_Condemnation = [
                            "I always feel like i'm not good enough for jesus ",
                            "Psalm 103:8-14 NIV The Lord is compassionate and gracious slow to anger, abounding in love.9 He will not always accuse, nor will he harbor his anger forever; 10 he does not treat us as our sins deserve or repay us according to our iniquities. 11 For as high as the heavens are above the earth, so great is his love for those who fear him; 12 as far as the east is from the west, so far has he removed our transgressions from us. 13 As a father has compassion on his children, so the Lord has compassion on those who fear him; 14 for he knows how we are formed, he remembers that we are dust.",
                            "I feel like god always scolds me ",
                            "Romans 8:14 NIV For those who are led by the Spirit of God are the children of God."
]

# train custom list patterns
list_trainer = ListTrainer(bot)
list_trainer.train(listToTrain1_Love)
list_trainer.train(listToTrain_aboutGod)
list_trainer.train(listToTrain_Fear)
list_trainer.train(listToTrain_Depression)
list_trainer.train(listToTrain_Condemnation)

#This is general statements and answers training for chatbot corpus training
trainer = ChatterBotCorpusTrainer(bot)
trainer.train("chatterbot.corpus.english")


@app.route("/")
def main():
    return render_template("index.html")

@app.route("/get")
def get_chatbot_response():
    userText = request.args.get('userMessage')
    return str(bot.get_response(userText))

if __name__ == "__main__":
    app.run(debug=True)
